\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Logiciels utilis\IeC {\'e}s}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Pr\IeC {\'e}sentation de LXC}{5}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Namespace}{6}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Cgroup}{6}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}chroot}{7}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}apparmor}{7}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}seccomp}{7}{subsubsection.2.1.5}
\contentsline {subsubsection}{\numberline {2.1.6}Kernel Capabilities}{7}{subsubsection.2.1.6}
\contentsline {subsection}{\numberline {2.2}Pr\IeC {\'e}sentation de LXD}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Pr\IeC {\'e}sentation de snapd}{8}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Pr\IeC {\'e}sentation de YunoHost}{8}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Pr\IeC {\'e}sentation de HAProxy}{10}{subsection.2.5}
\contentsline {section}{\numberline {3}Travail r\IeC {\'e}alis\IeC {\'e}}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Probl\IeC {\`e}mes r\IeC {\'e}seaux}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Installation de YunoHost}{12}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Les certificats}{12}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Le service Avahi}{13}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Cr\IeC {\'e}ation d'une image LXC personnalis\IeC {\'e}e}{13}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Environnement de test}{14}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Reverse-proxy web}{14}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Gestion des certificats}{15}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Recr\IeC {\'e}ation des certificats}{16}{subsection.3.9}
\contentsline {subsection}{\numberline {3.10}Script pour le provisionning des conteneurs et la configuration de HAProxy}{17}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}R\IeC {\'e}sum\IeC {\'e} du travail effectu\IeC {\'e}}{19}{subsection.3.11}
\contentsline {section}{\numberline {4}Prolongements possibles du projet}{20}{section.4}
\contentsline {section}{\numberline {5}Gestion du projet}{21}{section.5}
\contentsline {subsection}{\numberline {5.1}Moyens de communication et de partage}{21}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Diagramme de GANTT}{21}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}R\IeC {\'e}partion des t\IeC {\^a}ches}{22}{subsection.5.3}
\contentsline {section}{\numberline {6}Conclusion}{23}{section.6}
\contentsline {section}{\numberline {7}Bibliographie}{24}{section.7}
\contentsline {section}{\numberline {8}Annexe}{26}{section.8}
\contentsline {subsection}{\numberline {8.1}Configuration d'HAProxy}{26}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Script pour le provisionning des conteneurs et la configuration de haproxy}{29}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Script de recr\IeC {\'e}ation des certificats}{35}{subsection.8.3}
