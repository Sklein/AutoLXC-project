\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Probl\IeC {\'e}matique et Contexte}{3}{subsection.1.1}
\contentsline {section}{\numberline {2}LXC}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Introduction \IeC {\`a} LXC}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Fonctionnement de LXC}{6}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Namespace}{6}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Cgroup}{6}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}chroot}{7}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}apparmor}{7}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}seccomp}{7}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Kernel Capabilities}{8}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Probl\IeC {\`e}mes r\IeC {\'e}seaux}{8}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Fonctionnement de LXD}{8}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Installation de LXD}{8}{subsubsection.2.3.2}
\contentsline {section}{\numberline {3}Yunohost}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Introduction \IeC {\`a} Yunohost}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Installation de Yunohost}{11}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Le service Avahi}{11}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Les certificats}{12}{subsection.3.4}
\contentsline {section}{\numberline {4}HAProxy}{13}{section.4}
\contentsline {subsection}{\numberline {4.1}Pr\IeC {\'e}sentation d'HAProxy}{13}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Reverse-proxy web}{13}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Gestion des certificats}{15}{subsection.4.3}
\contentsline {section}{\numberline {5}Travail r\IeC {\'e}alis\IeC {\'e}}{16}{section.5}
\contentsline {subsection}{\numberline {5.1}Les templates}{16}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Environnement de test}{16}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Le script}{17}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Recr\IeC {\'e}ation des certificats}{19}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}R\IeC {\'e}sum\IeC {\'e} du travail effectu\IeC {\'e}}{19}{subsection.5.5}
\contentsline {section}{\numberline {6}Prolongement}{21}{section.6}
\contentsline {section}{\numberline {7}Planification}{22}{section.7}
\contentsline {subsection}{\numberline {7.1}Moyens de communication et de partage}{22}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Diagramme de GANTT}{22}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}R\IeC {\'e}partion des t\IeC {\^a}ches}{22}{subsection.7.3}
\contentsline {section}{\numberline {8}Conclusion}{24}{section.8}
\contentsline {section}{\numberline {9}Bibliographie}{25}{section.9}
\contentsline {section}{\numberline {10}Annexe}{26}{section.10}
\contentsline {subsection}{\numberline {10.1}Configuration d'HAProxy}{26}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Script de cr\IeC {\'e}ation de conteneur}{27}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Script de recr\IeC {\'e}ation de certificat}{28}{subsection.10.3}
